# IoMT node on STM32L4 microcontroller
A software solution is proposed to manage an IoMT node at run-time and in real-time. The platform used in this project is the SensorTile developed by ST microelectronics, it is composed of the STM32L476JG microcontroller and some sensory or communication modules.

## Operating system
In order to manage the system in a modular way and to respect real-time constraints in a precise way, FreeRTOS was chosen as the operating system. The main typical tools of an RTOS that have been used are:
- Thread
- Message
- Semaphore
- Timer

## System description
The system is able to monitor in real time the data taken from various connected sensors, the data can possibly be processed within the node and then sent to the gateway if necessary. We selected an application structure based on process networks. Tasks are represented as independent processes, communicating with each other via FIFO structures. For each sensed variable to be monitored, we build a chain of tasks that operate on the sensed data, so that, if required by changes in the operating mode, it's possible to dynamically turn on and off the useful and non-useful task components.
Four types of tasks have been considered:
- *Get thread:* it takes care of taking data from the sensing hardware integrated in the node.
- *Process thread:* it's possible to have multiple tasks of this type, representing multiple stages of in-place data analysis algorithm.
- *Threshold  task:* it takes care of preparing and filtering the output data .
- *Send thread:* takes care of sending the data to the gateway.

## Code description
In this session there is a generic description of the code and how to properly modify it.

### General function
In this particular case, the sensors taken into consideration are three, the structure of task chains per sensor is visible in line 1043. All the threads that a sensor has available are defined, in fact, not all threads are neccesarily active, there it is a strong dependence on the operating mode with the single sensor. When a thread has no task to perform, the operating system puts them in a quiescent state, if all the threads are in a quiescent state then the microcontroller is set to a state of low energy consumption, the operating system will program the the system awakening according to the scheduling.

Threads can be awakened in two different ways, if there is a message in the thread entry FIFO or through a periodic timer.

| Thread            | Awakening via FIFO    | Awakening via timer   |
| ------            | ------                | ------                |
| Get thread          | ✗                     | ✓                     |
| Process thread      | ✓                     | ✗                     |
| Threshold thread    | ✓                     | ✓                     |
| Send thread         | ✓                     | ✗                     |

*Get thread* is awakened by a periodic timer in order to respect the timing dictated by the chosen sampling frequency. *Get data* is awakened by a periodic timer to be able to respect the timing dictated by the chosen sampling frequency and does not wait for input data. Unlike the latter, all the other threads remain in a state of quescence as long as there is data available in the incoming FIFO.
*Threshold thread* has the particularity of being able to be awakened both via FIFO and via timer, so as to give the possibility to choose between: sending data whenever a data becomes available or sending data every *Δt*.

### Master thread
Line 839 shows the definition of the thread *master*, in the same way as the threshold thread, the master thread can also be activated both via FIFO and via timer.

| Thread            | Awakening via FIFO    | Awakening via timer   |
| ------            | ------                | ------                |
| Master thread    | ✓                     | ✓                     |

Messages that can arrive via FIFO can be of two types. The first is of type TASK_SETUP and the second is ti of type PERIOD_SETUP.

###### TASK_SETUP
Whenever this type of message arrives, for each sensor (line 875) the operating mode chosen for that sensor (line 877) is evaluated and the system is reconfigured. In paticoare, the timers (e.g. line 891, 892) are activated or deactivated, message traffic is rerouted with the correct FIFO (e.g. line 894, 895), the number of packages to be merged is defined and then sent (e.g. . 897,898) and the threads are activated or deactivated (e.g. line 900-904). All these choices are made consistently with the required operating mode.

###### PERIOD_SETUP
There are two types of timers, the timers that trigger the sampling on the enabled sensors (line 956) and the timers that trigger the sending of the packages to the thread that is responsible for exchanging data with the gateway (line 963).

###### 
The messages that come from a timer are of the PERIOD_CHECK type.

###### PERIOD_CHECK
This type of message (line 970) to the master thread allows periodic checks on the system, for example to evaluate the battery level in order to send a warning message or allows to self-reconfigure themselves to consume less energy.

### Modularity function
The code has been structured so as to simplify the integration of new sensors, the following table shows the parts of the code to be populated to allow the insertion of a new sensor. So as not to be repetitive, whenever an object containing _n is found, it refers to the n-th sensor, therefore if you want to add a fourth sensor, you will have to replicate the objects that are described in the table having _3 with _4.

|   Code lines   | Description |
| ---------- | ------ |
| 55-140     | *typedef* where identifiers used by operating system tools are specified (such as threads or timers) |
| 413-497     | From **416** to **420** there is the definition of threads. From **422** to **433** there is the definition of the operating system tools. From **435** to **436** there is the definition of the timers. From **440** onwards there are the variables and functions useful for the specific sensor. |
| 664-716     | The structure described in line 209 is populated, in line **669** the type of tado that is taken from the sensor must be specified based on those available in the structure defined in 172, it is useful for malloc functions. In line **670** the type of sensor must be specified from the point of view of the structure of the data that is collected, these types are specified in the structure at line 164, this will be used to understand the amount of data extrapolated from a sensor. In lines **673-677** it is specified which threads are used by the sensor. The **706-710** lines specify the specific functions for the sensor.|
| 1041-151     | These lines of code define the threads to be called by the system and the functions associated with them. The functions can be generic (they are defined from 1087 to 1190) or they can be manually imported into the code (in this case they are defined from 1200 to 1358).|

## To do
...